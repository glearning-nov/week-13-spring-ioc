package com.glearning.spring.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.glearning.spring.model.UberGo;

public class PassengerClient {
	
	public static void main(String[] args) throws InterruptedException {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
		
		/*
		 * Passenger passenger = applicationContext.getBean("passenger",
		 * Passenger.class);
		 * 
		 * passenger.travel("BTM-Layout", "Adugodi");
		 * 
		 * passenger.getDriver().printDriverDetails();
		 */
		
		/*
		 * UberGo uberGo = applicationContext.getBean("uberGo", UberGo.class);
		 * 
		 * uberGo.printDriverDetails();
		 */
		
		/*
		 * String [] beans = applicationContext.getBeanDefinitionNames();
		 * Stream.of(beans).forEach(bean -> System.out.println(bean));
		 */
		
		UberGo uberGo1 = applicationContext.getBean("uberGo", UberGo.class);

		
		UberGo uberGo2 = applicationContext.getBean("uberGo", UberGo.class);
		
		
		System.out.println("uberGo1 == uberGo2 "+ (uberGo1 == uberGo2));
		
		uberGo2.printDriverDetails();
		
		uberGo1.setName("Ramesh Kumar");
		
		uberGo2.printDriverDetails();
		
		
		
		AbstractApplicationContext context = (AbstractApplicationContext)applicationContext;
		context.close();
		context.registerShutdownHook();
		
		Thread.sleep(4000);
	}

}
