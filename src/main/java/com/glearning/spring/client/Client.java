package com.glearning.spring.client;

import com.glearning.spring.model.Commute;
import com.glearning.spring.model.Passenger;
import com.glearning.spring.model.UberGo;
import com.glearning.spring.model.UberPrime;

public class Client {
	
	public static void main(String[] args) {
		
		/*
		 * 1. The client is responsible for instantiating the objects
		 * 2. The client is responsible for doing the dependency injection
		 */
		Commute driver = new UberGo("Rakesh", "Waganor");
		Commute uberPrimeDriver = new UberPrime("Vignesh", "Ciaz");
		
		//The client is responsible for doing the dependency injection
		Passenger passenger = new Passenger(uberPrimeDriver);
		
		passenger.travel("Banashakari", "KIAL");
	}

}
