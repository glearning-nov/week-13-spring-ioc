package com.glearning.spring.model;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {
	
	@Bean
	public UberPrime primeDriver() {
		return new UberPrime("Santosh", "Elantra");
	}

}
