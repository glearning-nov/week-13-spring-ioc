package com.glearning.spring.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Passenger {
	
	@Autowired
	private Commute driver;
	
	public Passenger(@Qualifier("uberGo") Commute driver) {
		this.driver = driver;
	}
	
	public void travel(String from, String to) {
		this.driver.trip(from, to);
	}
	
	public Commute getDriver() {
		return this.driver;
	}

}
