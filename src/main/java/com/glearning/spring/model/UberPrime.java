package com.glearning.spring.model;

import org.springframework.stereotype.Component;

@Component
public class UberPrime implements Commute {
	
	private String name;
	private String vehicle;
	
	public UberPrime() {}
	
	public UberPrime(String name, String vehicle) {
		this.name = name;
		this.vehicle = vehicle;
	}

	public void trip(String from, String destination) {
		System.out.println("Commuting with Uber-Prime from "+from + " to "+ destination +" on a Seadan");
	}
	
	public void printDriverDetails() {
		System.out.println("Name of the driver:: "+ name);
		System.out.println("Vehicle details :: "+ vehicle);
	}


}
