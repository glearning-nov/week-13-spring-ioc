package com.glearning.spring.model;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Primary
@Scope("singleton")
public class UberGo implements Commute {
	
	@Value(value = "Ramesh")
	private String name;
	
	@Value(value = "Ritz")
	private String vehicle;
	
	public UberGo() {}
	
	public UberGo(String name, String vehicle) {
		this.name = name;
		this.vehicle = vehicle;
	}
	
	@PostConstruct
	public void init() {
		System.out.println("Performing custom initialization");
	}
	
	@PreDestroy
	public void tearDown() {
		System.out.println("Performing custom tear down operation");
	}


	public void trip(String from, String destination) {
		System.out.println("Commuting with Uber-Go from "+from + " to "+ destination +" on a Hatchback");
	}

	public void printDriverDetails() {
		System.out.println("Name of the driver:: "+ name);
		System.out.println("Vehicle details :: "+ vehicle);
	}
	
	public void setName(String name) {
		this.name = name;
	}

}
