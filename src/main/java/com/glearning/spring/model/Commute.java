package com.glearning.spring.model;

public interface Commute {
	
	void trip(String from, String destination);
	
	void printDriverDetails();

}
